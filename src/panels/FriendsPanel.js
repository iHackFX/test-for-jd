import React from "react";
import PropTypes from "prop-types";
import FriendsList from "../components/friends";
import {
  Group,
  Panel,
  Spacing,
} from "@vkontakte/vkui";
import "./Profile.css";
import "./Friends.css";
import { FriendsHeader } from "../components/header";

const FriendsPanel = ({ id, go }) => {
  return (
    <Panel id={id}>
      <Group header={<FriendsHeader go={go} />}>
        <FriendsList />
      </Group>
      <Spacing size={48} />
    </Panel>
  );
};

FriendsPanel.propTypes = {
  id: PropTypes.string.isRequired,
  go: PropTypes.func.isRequired,
};

export default FriendsPanel;
