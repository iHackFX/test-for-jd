import React from "react";
import PropTypes from "prop-types";
import { Icon24Add } from "@vkontakte/icons";
import { Panel, Group, Header, Spacing, CellButton } from "@vkontakte/vkui";
import HeaderWithIcon from "../components/header";

import "./Profile.css";
import FlashLightCell from "../components/FlashLightCell";
import ProfileCell from "../components/ProfileCell";
import FriendsList from "../components/friends";

const ProfilePanel = ({ id, go, fetchedUser, setScheme, ColorScheme }) => {
  return (
    <Panel id={id}>
      <Group
        header={
          <HeaderWithIcon setScheme={setScheme} ColorScheme={ColorScheme} />
        }
      >
        {fetchedUser && (
          <ProfileCell fetchedUser={fetchedUser} ColorScheme={ColorScheme} />
        )}
      </Group>
      <Group>
        <FlashLightCell />
      </Group>
      <Group header={<Header>Друзья</Header>} indicator="7">
        <FriendsList length={4} />
        <CellButton
          onClick={() => go("FriendsPanel")}
          centered
          before={<Icon24Add />}
        >
          Показать всех друзей
        </CellButton>
      </Group>
      <Spacing size={48} />
    </Panel>
  );
};

ProfilePanel.propTypes = {
  id: PropTypes.string.isRequired,
  go: PropTypes.func.isRequired,
  fetchedUser: PropTypes.shape({
    photo_200: PropTypes.string,
    first_name: PropTypes.string,
    last_name: PropTypes.string,
    city: PropTypes.shape({
      title: PropTypes.string,
    }),
    setScheme: PropTypes.func.isRequired,
    ColorScheme: PropTypes.string.isRequired,
  }),
};

export default ProfilePanel;
