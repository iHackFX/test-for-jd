import { Button, Group, Panel, Placeholder, Spacing } from "@vkontakte/vkui";
import PropTypes from "prop-types";
import HeaderWithIcon from "../components/header";
import ButtonsModalCard from "../components/ModalCard";
import PlaceHolderImage from "../images/placeholder.svg";
import "./PlaceHolder.css";

const PlaceHolderPanel = ({
  id,
  go,
  setScheme,
  ColorScheme,
  setActiveModal,
}) => {
  return (
    <Panel id={id}>
      <Group>
        <HeaderWithIcon setScheme={setScheme} ColorScheme={ColorScheme} />
        <Spacing size={150} />
        <Placeholder
          style={{ display: "flex", alignItems: "center" }}
          icon={<img src={PlaceHolderImage} />}
          header="Немного лирики"
          action={
            <Button
              onClick={() => {
                setActiveModal("ButtonsModalCard");
              }}
              size="m"
            >
              Нажми на меня
            </Button>
          }
        >
          Прежде чем описание станет хорошим, его необходимо написать. <br /> Не
          правда ли?
        </Placeholder>
        <Spacing size={150} />
      </Group>
    </Panel>
  );
};

PlaceHolderPanel.propTypes = {
  id: PropTypes.string.isRequired,
  go: PropTypes.func.isRequired,
  setScheme: PropTypes.func.isRequired,
  setActiveModal: PropTypes.func.isRequired,
  ColorScheme: PropTypes.string.isRequired,
};

export default PlaceHolderPanel;
