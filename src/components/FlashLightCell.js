import { Icon28LightbulbOutline } from "@vkontakte/icons";
import { RichCell, Switch } from "@vkontakte/vkui";
import { useEffect, useState } from "react";
import { getFlashLightInfo, setFlashLightLevel } from "../bridge/flashlight";



const FlashLightCell = () => {
    var [flashlightState, setFlashState] = useState(0);
    var [flashLightAval, setFlashAval] = useState(false);
    
    useEffect(async () => {
        setFlashAval(await getFlashLightInfo());
    }, [true]);    
    
    return (<RichCell
    className="center"
    before={<Icon28LightbulbOutline fill="#2975CC" />}
    after={
      <Switch
        disabled={!flashLightAval}
        onClick={(e) => {
          setFlashLightLevel(flashlightState ? 0 : 1);
          setFlashState(!flashlightState);
        }}
      />
    }
    caption={
      flashLightAval
        ? "На телефоне включится фонарик"
        : "Функция не поддерживается"
    }
  >
    Больше света!
  </RichCell>);
}

export default FlashLightCell;