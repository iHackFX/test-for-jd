import { List } from "@vkontakte/vkui";
import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import FriendCell from "./friendCell";

const FriendsList = ({ length }) => {
  var [friendsData, setFriendsData] = useState([]);

  function getFriends() {
    var data = [];
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((res) => res.json())
      .then(
        (result) => {
          data = result;
          setFriendsData(data);
        },
        (error) => {
          console.log(error);
        }
      );
  }
  useEffect(async () => {
    setFriendsData(getFriends());
  }, [true]);

  return (
    <List>
      {friendsData !== undefined
        ? friendsData.map((val, idx) => {
            if (idx >= length) {
              return;
            }
            return <FriendCell key={idx} user={val} />;
          })
        : ""}
    </List>
  );
};

FriendsList.propTypes = {
  length: PropTypes.number,
};

export default FriendsList;
