import { Icon28NameTagOutline, Icon56Stars3Outline, Icon28BillSeparatedOutline, Icon28IncognitoOutline } from "@vkontakte/icons";
import { Button, ModalCard, ModalRoot, RichCell } from "@vkontakte/vkui";
import PropTypes from "prop-types";
import "./ModalCard.css";

const ButtonsModalCard = ({ setActiveModal, id }) => {
  return (
    <ModalCard
      id={id}
      onClose={() => setActiveModal(null)}
      icon={<Icon56Stars3Outline />}
      header="Это модальное окно"
      subheader="Короткое описание, а может и не короткое"
      actions={
        <Button size="l" mode="primary" onClick={() => setActiveModal(null)}>
          Понятно
        </Button>
      }
    >
      <RichCell
        before={<Icon28NameTagOutline fill="#2975CC" style={{paddingRight: 10}} />}
        caption="Короткое описание"
      >
        Number one
      </RichCell>
      <RichCell
        before={<Icon28BillSeparatedOutline fill="#2975CC" style={{paddingRight: 10}} />}
        caption="Короткое описание"
      >
        Number two
      </RichCell>
      <RichCell
        before={<Icon28IncognitoOutline fill="#2975CC" style={{paddingRight: 10}} />}
        caption="Елочка гори"
      >
        Number three
      </RichCell>
    </ModalCard>
  );
};

ButtonsModalCard.propTypes = {
  id: PropTypes.string,
  setActiveModal: PropTypes.func.isRequired,
};

export default ButtonsModalCard;
