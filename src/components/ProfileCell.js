import PropTypes from "prop-types";
import { Icon20HomeOutline, Icon20UserOutline } from "@vkontakte/icons";
import { Avatar, Div, Link, MiniInfoCell, Placeholder, Text } from "@vkontakte/vkui";

const ProfileCell = ({ fetchedUser }) => {
  return (
    <Placeholder
      icon={
        fetchedUser.photo_200 ? (
          <Avatar size={98} src={fetchedUser.photo_200} />
        ) : null
      }
      header={fetchedUser.first_name + " " + fetchedUser.last_name}
    >
      <Div className="center disablePadding disableMargin">
        {fetchedUser.city ? (
          <MiniInfoCell
            before={<Icon20HomeOutline className="disableMargin" />}
            textWrap="full"
            textLevel="primary"
            className="center"
          >
            <Text>{fetchedUser.city.title}</Text>
          </MiniInfoCell>
        ) : null}
        <Link href={"https://vk.com/id" + fetchedUser.id} target="_blank">
          <MiniInfoCell
            before={<Icon20UserOutline className="link" />}
            textWrap="full"
            textLevel="primary"
            className="center"
          >
            <Text className="link">Открыть профиль</Text>
          </MiniInfoCell>
        </Link>
      </Div>
    </Placeholder>
  );
};

ProfileCell.propTypes = {
    fetchedUser: PropTypes.shape({
        photo_200: PropTypes.string,
        first_name: PropTypes.string,
        last_name: PropTypes.string,
        city: PropTypes.shape({
          title: PropTypes.string,
        })
    }).isRequired
};

export default ProfileCell;
