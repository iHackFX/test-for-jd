import {
  Icon24ChevronLeft,
  Icon24LogoVkColor,
  Icon28MoonOutline,
  Icon28SunOutline,
} from "@vkontakte/icons";
import { Div, IconButton, Title } from "@vkontakte/vkui";
import PropTypes from "prop-types";

import "./header.css";
const EmptyImage = "data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=";
const HeaderWithIcon = ({ ColorScheme, setScheme }) => (
  <Div className="HeaderWithIcon">
    <IconButton
      onClick={() => {
        setScheme(ColorScheme === "vkcom_light" ? "vkcom_dark" : "vkcom_light");
      }}
    >
      {ColorScheme === "vkcom_light" ? (
        <Icon28MoonOutline fill="#2975CC" />
      ) : (
        <Icon28SunOutline fill="#2975CC" />
      )}
    </IconButton>
    <div className="HeaderWithIcon">
      <Icon24LogoVkColor />
      <Title weight="1">ui</Title>
    </div>
    <img height="28" width="28" src={EmptyImage} />
  </Div>
);

const FriendsHeader = ({ go }) => (
  <Div className="HeaderWithIcon">
    <IconButton
      onClick={() => {
        go("Profile");
      }}
    >
      <Icon24ChevronLeft fill="#2975CC" />
    </IconButton>
    <div className="HeaderWithIcon">
      <Title weight="1">Друзья</Title>
    </div>
    <img height="24" width="24" src={EmptyImage} />
  </Div>
);

HeaderWithIcon.propTypes = {
  setScheme: PropTypes.func.isRequired,
};
FriendsHeader.propTypes = {
  go: PropTypes.func.isRequired,
  ColorScheme: PropTypes.string.isRequired,
  setScheme: PropTypes.func.isRequired,
};
export default HeaderWithIcon;
export { HeaderWithIcon, FriendsHeader };
