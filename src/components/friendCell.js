import { calcInitialsAvatarColor, InitialsAvatar, RichCell } from "@vkontakte/vkui";
import PropTypes from "prop-types";

const FriendCell = ({ user }) => {
  return (
    <RichCell
      before={
        <InitialsAvatar
          size={48}
          gradientColor={calcInitialsAvatarColor(user.id)}
        >
          {user.name
            .split(" ")
            .map((n) => n[0])
            .join(" ")}
        </InitialsAvatar>
      }
      caption={
        <div>
          {user.email} <br />
          {user.phone}
        </div>
      }
    >
      {user.name}
    </RichCell>
  );
};

FriendCell.propTypes = {
    user: PropTypes.shape({
        name: PropTypes.string,
        email: PropTypes.string,
        phone: PropTypes.string
      }).isRequired
};

export default FriendCell;
