import React, { useState, useEffect } from "react";
import bridge from "@vkontakte/vk-bridge";
import {
  View,
  ScreenSpinner,
  AdaptivityProvider,
  AppRoot,
  ConfigProvider,
  SplitLayout,
  SplitCol,
  Tabbar,
  TabbarItem,
  ModalRoot,
} from "@vkontakte/vkui";
import "@vkontakte/vkui/dist/vkui.css";
import Icon28NewsfeedLinesOutline from "./images/newsfeed_lines_outline.svg";
import Icon28NewsfeedLinesOutlineActive from "./images/newsfeed_lines_outline_active.svg";
import IconVmojiOutline28 from "./images/vmoji_outline.svg";
import IconVmojiOutline28Active from "./images/vmoji_outline_active.svg";
import ProfilePanel from "./panels/ProfilePanel";
import FriendsPanel from "./panels/FriendsPanel";
import PlaceHolderPanel from "./panels/PlaceHolderPanel";
import ButtonsModalCard from "./components/ModalCard";

const App = () => {
  const [scheme, setScheme] = useState("vkcom_light");
  const [activePanel, setActivePanel] = useState("Profile");
  const [fetchedUser, setUser] = useState(null);
  const [popout, setPopout] = useState(<ScreenSpinner size="large" />);
  const [activeModal, setActiveModal] = useState(null);

  useEffect(() => {
    bridge.subscribe(({ detail: { type, data } }) => {
      if (type === "VKWebAppUpdateConfig") {
        setScheme(data.scheme);
      }
    });

    async function fetchData() {
      const user = await bridge.send("VKWebAppGetUserInfo");
      setUser(user);
    }
    setPopout(null);
    fetchData();
  }, []);

  const go = (e) => {
    setActivePanel(e.currentTarget.dataset.to);
  };

  const modal = (
    <ModalRoot activeModal={activeModal} onClose={()=>setActiveModal(null)}> 
      <ButtonsModalCard id={"ButtonsModalCard"} setActiveModal={setActiveModal} />
    </ModalRoot>
  )

  return (
    <ConfigProvider scheme={scheme}>
      <AdaptivityProvider>
        <AppRoot>
          <SplitLayout popout={popout} modal={modal}>
            <SplitCol>
              <View activePanel={activePanel}>
                <ProfilePanel
                  id="Profile"
                  fetchedUser={fetchedUser}
                  go={setActivePanel}
                  setScheme={setScheme}
                  ColorScheme={scheme}
                />
                <PlaceHolderPanel
                  id="PlaceHolder"
                  go={setActivePanel}
                  setScheme={setScheme}
                  ColorScheme={scheme}
                  setActiveModal={setActiveModal}
                />
                <FriendsPanel id="FriendsPanel" go={setActivePanel} />
              </View>
            </SplitCol>
          </SplitLayout>
          <Tabbar>
            <TabbarItem
              selected={activePanel === "PlaceHolder"}
              text="PlaceHolder"
              onClick={() => setActivePanel("PlaceHolder")}
            >
              <img
                src={
                  activePanel === "PlaceHolder"
                    ? Icon28NewsfeedLinesOutlineActive
                    : Icon28NewsfeedLinesOutline
                }
              />
            </TabbarItem>
            <TabbarItem
              selected={activePanel === "Profile"}
              text="Профиль"
              onClick={() => setActivePanel("Profile")}
            >
              <img
                src={
                  activePanel === "Profile"
                    ? IconVmojiOutline28Active
                    : IconVmojiOutline28
                }
              />
            </TabbarItem>
          </Tabbar>
        </AppRoot>
      </AdaptivityProvider>
    </ConfigProvider>
  );
};

export default App;
