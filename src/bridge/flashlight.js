import bridge from "@vkontakte/vk-bridge";

const getFlashLightInfo = async () => {
    var a = await bridge
    .send("VKWebAppFlashGetInfo");
    return a.is_available; 
}

const setFlashLightLevel = (level) => {
  bridge
    .send("VKWebAppFlashSetLevel", {
      level: level,
    })
    .then((data) => {
      if (data.result) {
        // Уровень яркости фонарика установлен
      }
    })
    .catch((error) => {
      // Ошибка
      console.log(error);
    });
}

export { getFlashLightInfo, setFlashLightLevel };
